package com.senac.br.myapplication;

import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private boolean limpaDisplay = true;


    private TextView txtDisplay;
    private Button btn1;
    private Button btn2;
    private Button btn3;
    private Button btn4;
    private Button btn5;
    private Button btn6;
    private Button btn7;
    private Button btn8;
    private Button btn9;


    private void init() {
        int orientacao = this.getResources().getConfiguration().orientation;


        if (orientacao == Configuration.ORIENTATION_PORTRAIT) {
            Toast.makeText(this, "Orientacao Portrait", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Orientacao Landscape", Toast.LENGTH_LONG).show();
        }


        this.txtDisplay = findViewById(R.id.display);
        this.btn1 = findViewById(R.id.btn1);
        this.btn2 = findViewById(R.id.btn2);
        this.btn3 = findViewById(R.id.btn3);
        this.btn4 = findViewById(R.id.btn4);
        this.btn5 = findViewById(R.id.btn5);
        this.btn6 = findViewById(R.id.btn6);
        this.btn7 = findViewById(R.id.btn7);
        this.btn8 = findViewById(R.id.btn8);
        this.btn9 = findViewById(R.id.btn9);

        this.btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button button = (Button) view;
                String textoDoBotao = button.getText().toString();
                if (limpaDisplay) {
                    txtDisplay.setText(textoDoBotao);
                    limpaDisplay = false;
                } else {
                    txtDisplay.setText(txtDisplay.getText() + textoDoBotao);
                }

            }
        });
        this.btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button button = (Button) view;
                String textoDoBotao = button.getText().toString();
                if (limpaDisplay) {
                    txtDisplay.setText(textoDoBotao);
                    limpaDisplay = false;
                } else {
                    txtDisplay.setText(txtDisplay.getText() + textoDoBotao);
                }

            }
        });
        this.btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button button = (Button) view;
                String textoDoBotao = button.getText().toString();
                if (limpaDisplay) {
                    txtDisplay.setText(textoDoBotao);
                    limpaDisplay = false;
                } else {
                    txtDisplay.setText(txtDisplay.getText() + textoDoBotao);
                }

            }
        });
        this.btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button button = (Button) view;
                String textoDoBotao = button.getText().toString();
                if (limpaDisplay) {
                    txtDisplay.setText(textoDoBotao);
                    limpaDisplay = false;
                } else {
                    txtDisplay.setText(txtDisplay.getText() + textoDoBotao);
                }

            }
        });
        this.btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button button = (Button) view;
                String textoDoBotao = button.getText().toString();
                if (limpaDisplay) {
                    txtDisplay.setText(textoDoBotao);
                    limpaDisplay = false;
                } else {
                    txtDisplay.setText(txtDisplay.getText() + textoDoBotao);
                }

            }
        });
        this.btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button button = (Button) view;
                String textoDoBotao = button.getText().toString();
                if (limpaDisplay) {
                    txtDisplay.setText(textoDoBotao);
                    limpaDisplay = false;
                } else {
                    txtDisplay.setText(txtDisplay.getText() + textoDoBotao);
                }

            }
        });
        this.btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button button = (Button) view;
                String textoDoBotao = button.getText().toString();
                if (limpaDisplay) {
                    txtDisplay.setText(textoDoBotao);
                    limpaDisplay = false;
                } else {
                    txtDisplay.setText(txtDisplay.getText() + textoDoBotao);
                }

            }
        });
        this.btn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button button = (Button) view;
                String textoDoBotao = button.getText().toString();
                if (limpaDisplay) {
                    txtDisplay.setText(textoDoBotao);
                    limpaDisplay = false;
                } else {
                    txtDisplay.setText(txtDisplay.getText() + textoDoBotao);
                }

            }
        });
        this.btn9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button button = (Button) view;
                String textoDoBotao = button.getText().toString();
                if (limpaDisplay) {
                    txtDisplay.setText(textoDoBotao);
                    limpaDisplay = false;
                } else {
                    txtDisplay.setText(txtDisplay.getText() + textoDoBotao);
                }

            }
        });

    }
}